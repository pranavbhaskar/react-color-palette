import {useState, useEffect} from 'react';
import './App.css';

function generateColor() {
  let letters = '0123456789abcdef';
  let color = '#';

  for(let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function getPalette() {
  let palette = [];

  for(let i = 0; i < 12; i++) {
    palette.push(generateColor());
  }
  return palette;
}

function App() {
  let [palette, setPalette] = useState([]);
  let [bg, setBg] = useState('#eee');

  useEffect(() => { 
    newPalette();
  }, [])

  function newPalette() {
    let newPalette = getPalette();
    setPalette(newPalette);
  }

  return (
    <div className="App" style={{background: bg}}>
      <div className="wrapper">
      {palette.length && palette.map((c, i) => (
        <div 
          style={{
            width: '52px', 
            height: '52px', 
            background: c, 
            margin: '0 20px 20px 0',
            cursor: 'pointer',
            borderRadius: '5px'
          }}
          onClick={() => {setBg(palette[i])}}></div>
      ))}
      <button onClick={() => newPalette()}>new colors.. </button>
        <span>{bg === '#eee' ? 'Pick a color...' : bg}</span>
      </div>
    </div>
  );
}

export default App;
